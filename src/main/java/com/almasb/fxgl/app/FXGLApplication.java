package com.almasb.fxgl.app;

import com.almasb.fxgl.core.concurrent.Async;
import com.almasb.fxgl.core.concurrent.IOTask;
import com.almasb.fxgl.core.serialization.Bundle;
import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.profile.DataFile;
import com.almasb.fxgl.profile.SaveLoadHandler;
import com.almasb.fxgl.ui.ErrorDialog;
import com.almasb.fxgl.ui.FontType;
import com.almasb.sslogger.Logger;
import javafx.application.Application;
import javafx.stage.Stage;

public class FXGLApplication extends Application {

    public static GameApplication app;
    private static ReadOnlyGameSettings settings;

    static Engine engine;
    private MainWindow mainWindow;

    /**
     * This is the main entry point as run by the JavaFX platform.
     */
    @Override
    public void start(Stage stage) {
        // any exception on the JavaFX thread will be caught and reported
        Thread.setDefaultUncaughtExceptionHandler((thread, e) -> handleFatalError(e));

        GameApplication.log.debug("Initializing FXGL");

        engine = new Engine(settings);

        // after this call, all FXGL.* calls (apart from those accessing services) are valid
        FXGL.inject$spacecubos(engine, app, this);

        var startupScene = settings.getSceneFactory().newStartup();

        // get window up ASAP
        mainWindow = new MainWindow(stage, startupScene, settings);
        mainWindow.show();

        // TODO: possibly a better way exists of doing below
        engine.getEnvironmentVars$spacecubos().put("settings", settings);
        engine.getEnvironmentVars$spacecubos().put("mainWindow", mainWindow);

        // start initialization of services on a background thread
        // then start the loop on the JavaFX thread
        var task = IOTask.ofVoid(() -> {
            engine.initServices();
            postServicesInit();
        })
                .onSuccess(n -> engine.startLoop())
                .onFailure(e -> handleFatalError(e))
                .toJavaFXTask();

        Async.INSTANCE.execute(task);
    }

    private void postServicesInit() {
        initPauseResumeHandler();
        initSaveLoadHandler();
        initAndLoadLocalization();
        initAndRegisterFontFactories();

        // onGameUpdate is only updated in Game Scene
        FXGL.getGameScene().addListener(tpf -> engine.onGameUpdate(tpf));
    }

    private void initPauseResumeHandler() {
        if (!settings.isMobile()) {
            mainWindow.iconifiedProperty().addListener((obs, o, isMinimized) -> {
                if (isMinimized) {
                    engine.pauseLoop();
                } else {
                    engine.resumeLoop();
                }
            });
        }
    }

    private void initSaveLoadHandler() {
        FXGL.getSaveLoadService().addHandler(new SaveLoadHandler() {
            @Override
            public void onSave(DataFile data) {
                var bundle = new Bundle("FXGLServices");
                engine.write(bundle);
            }

            @Override
            public void onLoad(DataFile data) {
                var bundle = data.getBundle("FXGLServices");
                engine.read(bundle);
            }
        });
    }

    private void initAndLoadLocalization() {
        GameApplication.log.debug("Loading localizations");

        settings.getSupportedLanguages().forEach(lang -> {
            var bundle = FXGL.getAssetLoader().loadResourceBundle("languages/" + lang.getName().toLowerCase() + ".properties");
            FXGL.getLocalizationService().addLanguageData(lang, bundle);
        });

        FXGL.getLocalizationService().selectedLanguageProperty().bind(settings.getLanguage());
    }

    private void initAndRegisterFontFactories() {
        GameApplication.log.debug("Registering font factories with UI factory");

        var uiFactory = FXGL.getUIFactoryService();

        uiFactory.registerFontFactory(FontType.UI, FXGL.getAssetLoader().loadFont(settings.getFontUI()));
        uiFactory.registerFontFactory(FontType.GAME, FXGL.getAssetLoader().loadFont(settings.getFontGame()));
        uiFactory.registerFontFactory(FontType.MONO, FXGL.getAssetLoader().loadFont(settings.getFontMono()));
        uiFactory.registerFontFactory(FontType.TEXT, FXGL.getAssetLoader().loadFont(settings.getFontText()));
    }

    private boolean isError = false;

    private void handleFatalError(Throwable error) {
        if (isError) {
            // just ignore to avoid spamming dialogs
            return;
        }

        isError = true;

        // stop main loop from running as we cannot continue
        engine.stopLoop();

        // block with error dialog so that user can read the error
        new ErrorDialog(error).showAndWait();

        GameApplication.log.fatal("Uncaught Exception:", error);
        GameApplication.log.fatal("Application will now exit");

        exitFXGL();
    }

    public void exitFXGL() {
        GameApplication.log.debug("Exiting FXGL");

        if (engine != null && !isError)
            engine.stopLoopAndExitServices();

        GameApplication.log.debug("Shutting down background threads");
        Async.INSTANCE.shutdownNow();

        GameApplication.log.debug("Closing logger and exiting JavaFX");
        Logger.close();
        javafx.application.Platform.exit();
    }

    static void launchFX(GameApplication app, ReadOnlyGameSettings settings, String[] args) {
        FXGLApplication.app = app;
        FXGLApplication.settings = settings;
        launch(args);
    }

    static void customLaunchFX(GameApplication app, ReadOnlyGameSettings settings, Stage stage) {
        FXGLApplication.app = app;
        FXGLApplication.settings = settings;
        new FXGLApplication().start(stage);
    }
}
