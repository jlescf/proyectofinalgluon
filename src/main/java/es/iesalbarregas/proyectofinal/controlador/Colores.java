package es.iesalbarregas.proyectofinal.controlador;

import javafx.scene.paint.Color;

public class Colores {
    private static int[] coloresBloque = {(int) ((Math.random() * 3)+1), (int) ((Math.random() * 3)+1)};

    private static Color c = null;

    private static int estado = 0;

    public static void reRollColores(){
        coloresBloque = new int[]{(int) ((Math.random() * 3) + 1), (int) ((Math.random() * 3) + 1)};
    }

    public static int[] getColoresBloque(){
        return coloresBloque;
    }

    public static Color getC() {
        return c;
    }

    public static void setC(Color c) {
        Colores.c = c;
    }

    public static Color randomColor(){
        int random = (int) ((Math.random() * 3) + 1);
        Color[] colores = new Color[]{
            Color.RED, Color.BLUE, Color.GREEN
        };
        return colores[random-1];
    }

    public static int[] getColoresBloqueInverso(){
        return new int[]{coloresBloque[1], coloresBloque[0]};
    }

    public static int getEstado() {
        return estado;
    }

    public static void setEstado(int estado) {
        Colores.estado = estado;
    }
}
