package es.iesalbarregas.proyectofinal.controlador;

import javafx.scene.paint.Color;

public class Celda {
    protected double x;
    protected double y;
    protected boolean vacia;
    protected Color color;

    public Celda(){
        this(0.0, 0.0, true, null);
    }

    public Celda(double x, double y, boolean vacia, Color color) {
        this.x = x;
        this.y = y;
        this.vacia = vacia;
        this.color = color;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public boolean isVacia() {
        return vacia;
    }

    public void setVacia(boolean vacia) {
        this.vacia = vacia;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String toString() {
        return String.format("%f,%f", x, y);
    }
}
