package es.iesalbarregas.proyectofinal.controlador;

import com.almasb.fxgl.app.scene.FXGLMenu;
import com.almasb.fxgl.app.scene.MenuType;
import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.ui.FontType;
import com.gluonhq.attach.audio.AudioService;
import javafx.beans.binding.StringBinding;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.util.Optional;

public class GameMenu extends FXGLMenu {

    public GameMenu() {
        super(MenuType.GAME_MENU);

        CustButton btnPlaySolo = new CustButton("Continuar", () -> fireResume());
        CustButton btnSalir = new CustButton("Salir", () -> fireExitToMainMenu());

        var box = new VBox(15, btnPlaySolo, btnSalir);
        box.setAlignment(Pos.CENTER_LEFT);
        box.setTranslateX(100);
        box.setTranslateY(300);

        Optional<AudioService> service = AudioService.create();
        if (service.isPresent()){
            setCursorInvisible();
        }

        getContentRoot().getChildren().addAll(box);
    }

    @Override
    protected Button createActionButton(StringBinding stringBinding, Runnable runnable) {
        return new Button();
    }

    @Override
    protected Button createActionButton(String s, Runnable runnable) {
        return new Button();
    }

    @Override
    protected Node createBackground(double v, double v1) {
        return FXGL.getAssetLoader().loadTexture("gamemenu.jpg");
    }

    @Override
    protected Node createProfileView(String s) {
        return new Rectangle();
    }

    @Override
    protected Node createTitleView(String s) {
        return new Rectangle();
    }

    @Override
    protected Node createVersionView(String s) {
        return new Rectangle();
    }

    private static class CustButton extends StackPane {
        private String texto;
        private Runnable accion;

        private Text text;

        public CustButton(String texto, Runnable accion) {
            this.texto = texto;
            this.accion = accion;

            text = FXGL.getUIFactoryService().newText(texto, Color.BLACK, FontType.TEXT, 36);

            setAlignment(Pos.CENTER_LEFT);

            setOnMouseClicked(mouseEvent -> {
                accion.run();
            });
            getChildren().addAll(text);
        }

    }
}
