package es.iesalbarregas.proyectofinal.controlador;

import com.almasb.fxgl.app.scene.FXGLMenu;
import com.almasb.fxgl.app.scene.MenuType;
import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.ui.FontType;
import com.gluonhq.attach.audio.AudioService;
import javafx.beans.binding.StringBinding;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.util.Optional;

public class MainMenu extends FXGLMenu {

    public MainMenu() {
        super(MenuType.MAIN_MENU);

        CustomButton btnPlaySolo = new CustomButton("Jugar", () -> fireNewGame());
        //CustomButton btnMulti = new CustomButton("Jugar Online", () -> fireNewGame());
        //CustomButton btnOpciones = new CustomButton("Opciones", () -> {});
        CustomButton btnSalir = new CustomButton("Salir", () -> fireExit());

        var box = new VBox(15, btnPlaySolo, btnSalir);
        box.setAlignment(Pos.CENTER_LEFT);
        box.setTranslateX(100);
        box.setTranslateY(300);

        Optional<AudioService> service = AudioService.create();
        if (service.isPresent()){
            setCursorInvisible();
        }


        getContentRoot().getChildren().addAll(box);
    }

    @Override
    protected Button createActionButton(StringBinding stringBinding, Runnable runnable) {
        return new Button();
    }

    @Override
    protected Button createActionButton(String s, Runnable runnable) {
        return new Button();
    }

    @Override
    protected Node createBackground(double v, double v1) {
        return FXGL.getAssetLoader().loadTexture("menu.jpg");
    }

    @Override
    protected Node createProfileView(String s) {
        return new Rectangle();
    }

    @Override
    protected Node createTitleView(String s) {
        return new Rectangle();
    }

    @Override
    protected Node createVersionView(String s) {
        return new Rectangle();
    }

    private static class CustomButton extends StackPane {
        private String texto;
        private Runnable accion;

        private Text text;

        public CustomButton(String texto, Runnable accion) {
            this.texto = texto;
            this.accion = accion;

            text = FXGL.getUIFactoryService().newText(texto, Color.GREENYELLOW, FontType.TEXT, 36);

            setAlignment(Pos.CENTER_LEFT);

            setOnMouseClicked(mouseEvent -> {
                accion.run();
            });
            getChildren().addAll(text);
        }

    }
}
