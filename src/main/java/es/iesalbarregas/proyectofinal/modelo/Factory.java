package es.iesalbarregas.proyectofinal.modelo;

import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.entity.EntityFactory;
import com.almasb.fxgl.entity.SpawnData;
import com.almasb.fxgl.entity.Spawns;
import es.iesalbarregas.proyectofinal.controlador.Colores;
import es.iesalbarregas.proyectofinal.controlador.Tipos;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import static com.almasb.fxgl.dsl.FXGL.*;


public class Factory implements EntityFactory {

    //Factory de creacion del bloque jugable
    @Spawns("bloque")
    public Entity newBloque(SpawnData data){
        Color[] colores = new Color[]{
                Color.RED, Color.BLUE, Color.GREEN
        };
        int primero;
        int segundo;
        if(Colores.getEstado()==0 || Colores.getEstado()==1) {
            primero = Colores.getColoresBloque()[0] - 1;
            segundo = Colores.getColoresBloque()[1] - 1;
        }else{
            primero = Colores.getColoresBloqueInverso()[0] - 1;
            segundo = Colores.getColoresBloqueInverso()[1] - 1;
        }
        Rectangle mitadIzq = new Rectangle(25, 25, colores[primero]);
        mitadIzq.setStroke(Color.GRAY);

        Rectangle mitadDer = new Rectangle(25, 25, colores[segundo]);
        mitadDer.setStroke(Color.GRAY);
        if (Colores.getEstado()==1 || Colores.getEstado()==3){
            mitadDer.setTranslateY(25);
        }else{
            mitadDer.setTranslateX(25);
        }

        return entityBuilder()
                .type(Tipos.Tipo.BLOQUE)
                .from(data)
                .viewWithBBox(mitadIzq)
                .viewWithBBox(mitadDer)
                .collidable()
                .zIndex(0)
                .build();
    }

    //Factory de creacion del bloque no jugable
    @Spawns("bloqueInmovil")
    public Entity newBloqueInmovil(SpawnData data){

        Rectangle bloque = new Rectangle(24, 25, Colores.getC());
        bloque.setStroke(Color.GRAY);

        return entityBuilder()
                .type(Tipos.Tipo.BLOQUE_INMOVIL)
                .from(data)
                .viewWithBBox(bloque)
                .collidable()
                .build();
    }

    //Factory de creacion del recuadro de juego
    @Spawns("recuadro")
    public Entity newRecuadro(SpawnData data){

        Rectangle recuadro = new Rectangle(300, 625, Color.TRANSPARENT);
        recuadro.setStroke(Color.CYAN);

        return entityBuilder()
                .type(Tipos.Tipo.RECUADRO)
                .from(data)
                .viewWithBBox(recuadro)
                .collidable()
                .zIndex(0)
                .build();
    }
}
