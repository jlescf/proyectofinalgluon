package es.iesalbarregas.proyectofinal.modelo;


import com.almasb.fxgl.app.GameApplication;
import com.almasb.fxgl.app.GameSettings;
import com.almasb.fxgl.app.scene.FXGLMenu;
import com.almasb.fxgl.app.scene.SceneFactory;
import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import com.almasb.fxgl.texture.Texture;
import com.gluonhq.attach.audio.Audio;
import com.gluonhq.attach.audio.AudioService;
import es.iesalbarregas.proyectofinal.controlador.*;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.almasb.fxgl.dsl.FXGL.*;
import static com.almasb.fxgl.dsl.FXGL.spawn;

public class App extends GameApplication {

    private Celda[][] mapa;
    private boolean cayendo;

    @Override
    protected void initSettings(GameSettings gameSettings) {
        gameSettings.setTitle("Space cubos");
        gameSettings.setVersion("1.00");
        gameSettings.setWidth(500);
        gameSettings.setHeight(900);
        gameSettings.setAppIcon("icono.png");
        gameSettings.setMenuEnabled(true);
        gameSettings.setMenuKey(KeyCode.ESCAPE);
        gameSettings.setSceneFactory(new SceneFactory() {
            @Override
            public FXGLMenu newMainMenu() {
                return new MainMenu();
            }

            @Override
            public FXGLMenu newGameMenu() {
                return new GameMenu();
            }
        });
    }

    @Override
    protected void initInput() {
        // Controles para pc
        onKeyDown(KeyCode.A, () -> {
            if (!cayendo && getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() != 100) {
                double x = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() - 25;
                double y = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getY();
                if (getCelda(x, y).isVacia()) {
                    getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).translateX(-25);
                }
            }
        });
        onKeyDown(KeyCode.D, () -> {
            if(Colores.getEstado()==0 || Colores.getEstado()==2){
                if (!cayendo && getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() != 350) {
                    double x = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() + 50;
                    double y = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getY();
                    if (getCelda(x, y).isVacia()) {
                        getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).translateX(25);
                    }
                }
            }else{
                if (!cayendo && getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() != 375) {
                    double x = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() + 25;
                    double y = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getY();
                    if (getCelda(x, y).isVacia()) {
                        getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).translateX(25);
                    }
                }
            }
        });

        onKeyDown(KeyCode.Q, () -> {
            if(!cayendo && !estaEnElLimiteDerecho(getCeldaIzquierdaSingleton()) && getCeldaDerechaSingleton().isVacia()) {
                Celda cambio = getCeldaIzquierdaSingleton();
                getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).removeFromWorld();
                int nuevoEstado = Colores.getEstado();
                if (nuevoEstado++ == 4) {
                    Colores.setEstado(0);
                } else {
                    Colores.setEstado(nuevoEstado);
                }
                spawn("bloque", cambio.getX(), cambio.getY());
            }
        });
    }

    @Override
    protected void initGameVars(Map<String, Object> vars) {
        vars.put("Puntuacion", 0);
    }

    @Override
    protected void initUI() {
        addVarText("Puntuacion", 250, 50);
    }


    @Override
    protected void initGame() {
        cayendo = false;

        Optional<AudioService> service = AudioService.create();
        if(service.isPresent()) {
            // Para móvil
            Optional<Audio> background = service.get().loadMusic(getClass().getResource("/assets/music/background.mp3"));
            background.ifPresent(audio -> {
                audio.play();
                audio.setLooping(true);
            });
            crearControles();
        } else {
            // Desktop
            getAudioPlayer().loopMusic(getAssetLoader().loadMusic("background.mp3"));
        }
        getGameScene().setCursorInvisible();

        //Inicializar array
        inicializarArray();

        getGameScene().setBackgroundRepeat(getAssetLoader().loadImage("background.jpg"));
        getGameWorld().addEntityFactory(new Factory());
        spawn("recuadro", 100, 100);
        spawn("bloque", 225, 125);
        Colores.setEstado(0);

        //Crea obstaculos en el mapa
        int i = (int) ((Math.random() * 3) + 1);
        int cont = 1;
        while (i >= cont) {
            Celda celdaObstaculo = celdaRandom();
            if(celdaObstaculo.isVacia()){
                celdaObstaculo.setColor(Colores.randomColor());
                celdaObstaculo.setVacia(false);
                setCelda(celdaObstaculo);
                Colores.setC(celdaObstaculo.getColor());
                spawn("bloqueInmovil", celdaObstaculo.getX(), celdaObstaculo.getY());
            }
            cont++;
        }

        run(() -> {
            if(!cayendo) {
                getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).translateY(25);
            }
        }, Duration.seconds(0.2));
    }

    private Celda celdaRandom(){
        int random = (int) ((Math.random() * 11) + 1);
        double x = (25 * random) + 100;//Esto para la x de la celda
        random = (int) ((Math.random() * 15) + 1);
        double y = (25 * random) + 300;//Esto para la y de la celda
        return getCelda(x,y);
    }

    //Controles para movil
    private void crearControles() {
        /*
            Texture pausa = texture("pausa.png");

            HBox pausaMenu = new HBox(10, pausa);
            pausa.setTranslateX(0);
            pausa.setTranslateY(0);
            Node node_pausa = pausaMenu;
            getGameScene().addUINode(node_pausa);

            pausa.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    getGameController().gotoGameMenu();
                }
            });
         */
        Texture izq = texture("izquierda.png");
        Texture der = texture("derecha.png");

        HBox controles = new HBox(10, izq, der);
        controles.setTranslateX(150);
        controles.setTranslateY(750);

        Node node = controles;
        getGameScene().addUINode(node);

        izq.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if (!cayendo && getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() != 100) {
                    double x = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() - 25;
                    double y = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getY();
                    if (getCelda(x, y).isVacia()) {
                        getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).translateX(-25);
                    }
                }
            }
        });

        der.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(Colores.getEstado()==0 || Colores.getEstado()==2){
                    if (!cayendo && getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() != 350) {
                        double x = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() + 50;
                        double y = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getY();
                        if (getCelda(x, y).isVacia()) {
                            getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).translateX(25);
                        }
                    }
                }else{
                    if (!cayendo && getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() != 375) {
                        double x = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() + 50;
                        double y = getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getY();
                        if (getCelda(x, y).isVacia()) {
                            getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).translateX(25);
                        }
                    }
                }
            }
        });

        Texture girar = texture("girar.png");

        HBox botonGirar = new HBox(10, girar);
        girar.setTranslateX(400);
        girar.setTranslateY(400);
        Node node_girar = botonGirar;
        getGameScene().addUINode(node_girar);

        girar.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(!cayendo && !estaEnElLimiteDerecho(getCeldaIzquierdaSingleton()) && getCeldaDerechaSingleton().isVacia()) {
                    Celda cambio = getCeldaIzquierdaSingleton();
                    getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).removeFromWorld();
                    int nuevoEstado = Colores.getEstado();
                    if (nuevoEstado++ == 4) {
                        Colores.setEstado(0);
                    } else {
                        Colores.setEstado(nuevoEstado);
                    }
                    spawn("bloque", cambio.getX(), cambio.getY());
                }
            }
        });
    }

    private void inicializarArray() {
        mapa = new Celda[24][12];
        double xAxis = 100;
        double yAxis = 700;

        for (int i = 0; i < 24; i++) {
            for (int j = 0; j < 12; j++) {
                mapa[i][j] = new Celda(xAxis, yAxis, true, null);
                xAxis += 25;
            }
            xAxis = 100;
            yAxis -= 25;
        }
    }

    @Override
    protected void initPhysics() {
        //Colisión de un bloque y un bloque ya inmovil
        onCollision(Tipos.Tipo.BLOQUE, Tipos.Tipo.BLOQUE_INMOVIL, (bloque, bqIn) -> {
        if(Colores.getEstado()==0 || Colores.getEstado()==2) {

            if (hayColisionHorizontal(bloque)) {

                // Tratar celda izquierda
                Celda izquierda = getCeldaIzquierdaSingleton();
                tratarColisionCelda(izquierda, 0);

                // Tratar celda derecha
                Celda derecha = getCeldaDerechaSingleton();
                tratarColisionCelda(derecha, 1);

                // Elimina el bloque actual del tablero. Ya ha sido tratado
                bloque.removeFromWorld();

                //Eliminar las celdas del combo vertical de la izquierda
                //Cambiar nombre variables
                List<Celda> casillasCombo = getComboVertical(izquierda);
                cayendo = casillasCombo.size() >= 4;
                eliminarCasillasCombo(casillasCombo);

                //Eliminar las celdas del combo vertical de la derecha
                casillasCombo = getComboVertical(derecha);
                cayendo |= casillasCombo.size() >= 4;
                eliminarCasillasCombo(casillasCombo);

                //Eliminar casillas del combo horizontal
                casillasCombo = getComboHorizontal(derecha);
                cayendo |= casillasCombo.size() >= 4;
                eliminarCasillasCombo(casillasCombo);

                // Lanza nuevo bloque al tablero de juego
                if (!cayendo) {
                    lanzaNuevoBloque();
                }
            }
        }else {

            if (hayColisionVertical(bloque)) {
                // Tratar celda arriba
                Celda arriba = getCeldaArribaSingleton();
                tratarColisionCelda(arriba, 0);

                // Tratar celda abajo
                Celda abajo = getCeldaIzquierdaSingleton();
                tratarColisionCelda(abajo, 1);

                // Elimina el bloque actual del tablero
                bloque.removeFromWorld();

                //Eliminar las celdas del combo vertical de la izquierda
                List<Celda> casillasCombo = getComboVertical(arriba);
                cayendo = casillasCombo.size() >= 4;
                eliminarCasillasCombo(casillasCombo);

                //Eliminar casillas del combo horizontal
                casillasCombo = getComboHorizontal(arriba);
                cayendo |= casillasCombo.size() >= 4;
                eliminarCasillasCombo(casillasCombo);

                //Eliminar casillas del combo horizontal
                casillasCombo = getComboHorizontal(abajo);
                cayendo |= casillasCombo.size() >= 4;
                eliminarCasillasCombo(casillasCombo);

                // Lanza nuevo bloque al tablero de juego
                if (!cayendo) {
                    lanzaNuevoBloque();
                }
            }
        }

        });

        onCollision(Tipos.Tipo.BLOQUE_INMOVIL, Tipos.Tipo.BLOQUE_INMOVIL, (bloque, bqIn) -> {
            Celda bloqueColision = getCelda(bloque.getX(), bloque.getY());
            List<Celda> casillasComboVertical = getComboVertical(bloqueColision);
            eliminarCasillasCombo(casillasComboVertical);

            List<Celda> casillasComboHorizontal = getComboHorizontal(bloqueColision);
            eliminarCasillasCombo(casillasComboHorizontal);
        });
    }

    private void eliminarCasillasCombo(List<Celda> casillasCombo) {
        int cont = 0;
        if (casillasCombo.size()==4) {
            //cayendo = true;
            while (cont < casillasCombo.size()) {
                Celda borrar = casillasCombo.get(cont);
                getGameWorld().getEntitiesAt(new Point2D(borrar.getX(), borrar.getY())).get(0).removeFromWorld();
                borrar.setVacia(true);
                setCelda(borrar);
                cont++;
            }
            inc("Puntuacion", 100);
        }
    }

    private Celda getCeldaIzquierdaSingleton() {
        return getCelda(getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX(), getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getY());
    }

    private Celda getCeldaDerechaSingleton() {
        return getCelda(getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX() + 25, getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getY());
    }

    private Celda getCeldaArribaSingleton() {
        return getCelda(getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getX(), getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).getY()-25);
    }

    private void tratarColisionCelda(Celda celda, int indice) {

        // Obtiene el color del bloque a crear
        Color color = obtenerColor(indice);

        // Configura el color de dibujado
        Colores.setC(color);

        // Configura la celda
        celda.setColor(color);
        celda.setVacia(false);
        setCelda(celda);

        // Crea el bloque que reemplaza al del jugador.
        spawn("bloqueInmovil", celda.getX(), celda.getY());
    }

    public boolean estaEnElLimiteInferior(Celda celda) {
        return celda.getY() + 25 > 700;
    }

    public boolean estaEnElLimiteDerecho(Celda celda) {
        return celda.getX() + 25 > 375;
    }

    public Celda getCeldaInferior(Celda celda) {
        return this.getCelda(celda.getX(), celda.getY() + 25);
    }

    private void lanzaNuevoBloque() {
            Colores.reRollColores();
            Colores.setEstado(0);
            spawn("bloque", 225, 125);
    }

    private Color obtenerColor(int indice) {
        final Color[] colores = new Color[]{
                Color.RED, Color.BLUE, Color.GREEN
        };
        Color c;
        if(Colores.getEstado()==0 || Colores.getEstado()==1){
            c = colores[Colores.getColoresBloque()[indice] - 1];
        }else{
            c = colores[Colores.getColoresBloqueInverso()[indice] - 1];
        }
        return c;
    }

    public boolean hayColisionHorizontal(Entity bloque) {
        return !estaEnElLimiteInferior(getCeldaIzquierdaSingleton())
                &&
                (!getCeldaInferior(getCeldaIzquierdaSingleton()).isVacia() || !getCeldaInferior(getCeldaDerechaSingleton()).isVacia());
    }

    // Arreglar colisiones
    public boolean hayColisionVertical(Entity bloque) {
        return !estaEnElLimiteInferior(getCeldaIzquierdaSingleton())
                &&
                (!getCeldaInferior(getCeldaIzquierdaSingleton()).isVacia());
    }

    int c = 0;
    @Override
    protected void onUpdate(double tpf) {
        if(Colores.getEstado()==0 || Colores.getEstado()==2) {

            if (!cayendo && estaEnElLimiteInferior(getCeldaIzquierdaSingleton())) {

                //Tratar parte izquierda bloque al llegar al limite
                Celda izquierda = getCeldaIzquierdaSingleton();
                tratarColisionCelda(izquierda, 0);

                //Tratar parte derecha bloque al llegar al limite
                Celda derecha = getCeldaDerechaSingleton();
                tratarColisionCelda(derecha, 1);

                //Elimina el bloque
                getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).removeFromWorld();

                //Eliminar casillas del combo horizontal en la última fila
                List<Celda> casillasCombo = getComboHorizontal(getCelda(125, 700));
                cayendo = casillasCombo.size()==4;
                eliminarCasillasCombo(casillasCombo);

                //Lanza el nuevo bloque
                if(!cayendo) {
                    lanzaNuevoBloque();
                }
            }
        }else {
            if (!cayendo && estaEnElLimiteInferior(getCeldaIzquierdaSingleton())) {

                //Tratar parte izquierda bloque al llegar al limite
                Celda izquierda = getCeldaArribaSingleton();
                tratarColisionCelda(izquierda, 0);

                //Tratar parte derecha bloque al llegar al limite
                Celda derecha = getCeldaIzquierdaSingleton();
                tratarColisionCelda(derecha, 1);

                //Elimina el bloque
                getGameWorld().getSingleton(Tipos.Tipo.BLOQUE).removeFromWorld();

                //Eliminar casillas del combo horizontal en la última fila
                List<Celda> casillasCombo = getComboHorizontal(getCelda(125, 700));
                cayendo = casillasCombo.size()==4;
                eliminarCasillasCombo(casillasCombo);

                //Lanza el nuevo bloque
                if(!cayendo) {
                    lanzaNuevoBloque();
                }
            }
        }

        c++;
        if(cayendo && ((c%20) == 0)){
            cayendo = activarGravedad();
            // Hacer colisiones
            if(!cayendo) {
                lanzaNuevoBloque();
            }
        }

        llegadaTope();
        victoria();
    }

    private void llegadaTope() {
        int cont = 0;
        boolean tope = false;
        while (cont < 12 && !tope) {
            tope = !mapa[21][cont].isVacia();
            cont++;
        }
        if (tope) {
            showMessage("Derrota", () -> getGameController().gotoMainMenu());
        }
    }

    private void victoria(){
        if(geti("Puntuacion")>5000){
            showMessage("Victoria", () -> getGameController().gotoMainMenu());
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    //Cambiar celda de array (Celda)
    private void setCelda(Celda celda) {
        for (int i = 0; i < 24; i++) {
            for (int j = 0; j < 12; j++) {
                if (mapa[i][j].getX() == celda.getX() && mapa[i][j].getY() == celda.getY()) {
                    mapa[i][j] = celda;
                }
            }
        }
    }

    //Devolver celda del array (X,Y)
    private Celda getCelda(double x, double y) {
        Celda c = null;
        for (int i = 0; i < 24; i++) {
            for (int j = 0; j < 12; j++) {
                if (mapa[i][j].getX() == x && mapa[i][j].getY() == y) {
                    c = mapa[i][j];
                }
            }
        }
        return c;
    }

    //Devuelve la lista de celdas en las que se da un combo vertical
    private List<Celda> getComboVertical(Celda celda) {
        List<Celda> casillasCombo = new ArrayList<>();
        int j = (int) ((celda.getX() - 100) / 25);
        int cAzul=0, cVerde=0, cRojo=0;
        boolean combo = false;
        for (int i = 0; i < 24 && !combo; i++) {
            if(!mapa[i][j].isVacia()) {
                Color color = mapa[i][j].getColor();
                if (color.equals(Color.RED)) {
                    cAzul = 0;
                    cVerde = 0;
                    cRojo++;
                    if(cRojo==1){
                        casillasCombo.clear();
                    }
                } else if (color.equals(Color.BLUE)) {
                    cAzul++;
                    cVerde = 0;
                    cRojo = 0;
                    if(cAzul==1){
                        casillasCombo.clear();
                    }
                } else {
                    cAzul = 0;
                    cVerde++;
                    cRojo = 0;
                    if(cVerde==1){
                        casillasCombo.clear();
                    }
                }
            }else{
                cAzul = 0;
                cVerde = 0;
                cRojo = 0;
                casillasCombo.clear();
            }
            casillasCombo.add(mapa[i][j]);
            if(cuentaCuatro(cRojo,cAzul,cVerde)){
                combo = true;
            }
        }
        return casillasCombo;
    }

    //Devuelve la lista de celdas en las que se da un combo horizontal
    private List<Celda> getComboHorizontal(Celda celda) {
        List<Celda> casillasCombo = new ArrayList<>();
        int i = 24 - (int) ((celda.getY() - 100) / 25);
        int cAzul=0, cVerde=0, cRojo=0;
        boolean combo = false;
        for (int j = 0; j < 12 && !combo; j++) {
            if(!mapa[i][j].isVacia()) {
                Color color = mapa[i][j].getColor();
                if (color.equals(Color.RED)) {
                    cAzul = 0;
                    cVerde = 0;
                    cRojo++;
                    if(cRojo==1){
                        casillasCombo.clear();
                    }
                } else if (color.equals(Color.BLUE)) {
                    cAzul++;
                    cVerde = 0;
                    cRojo = 0;
                    if(cAzul==1){
                        casillasCombo.clear();
                    }
                } else {
                    cAzul = 0;
                    cVerde++;
                    cRojo = 0;
                    if(cVerde==1){
                        casillasCombo.clear();
                    }
                }
            }else{
                cAzul = 0;
                cVerde = 0;
                cRojo = 0;
                casillasCombo.clear();
            }
            casillasCombo.add(mapa[i][j]);
            if(cuentaCuatro(cRojo,cAzul,cVerde)){
                combo = true;
            }
        }
        return casillasCombo;
    }

    private boolean cuentaCuatro(int rojo, int azul, int verde){
        return rojo==4 || azul==4 || verde==4;
    }

    //Mira para todos los bloques si tienen uno debajo y si no caen
    private boolean activarGravedad(){
        int cont = 0;
        for (int i = 0; i < 24; i++) {
            for (int j = 0; j < 12; j++) {
                Celda gravedad = mapa[i][j];
                    if (!estaEnElLimiteInferior(gravedad) && !gravedad.isVacia() && getCeldaInferior(gravedad).isVacia()) {
                        getGameWorld().getEntitiesAt(new Point2D(gravedad.getX(), gravedad.getY())).get(0).removeFromWorld();
                        gravedad.setVacia(true);
                        setCelda(gravedad);
                        getCeldaInferior(gravedad).setVacia(false);
                        getCeldaInferior(gravedad).setColor(gravedad.getColor());
                        Colores.setC(gravedad.getColor());
                        spawn("bloqueInmovil", gravedad.getX(), getCeldaInferior(gravedad).getY());
                        cont++;
                    }
                }
            }
        return cont!=0;
        }

    }
