#ifndef __ES_IESALBARREGAS_PROYECTOFINAL_MODELO_APP_H
#define __ES_IESALBARREGAS_PROYECTOFINAL_MODELO_APP_H

#include <graal_isolate_dynamic.h>


#if defined(__cplusplus)
extern "C" {
#endif

typedef int (*run_main_fn_t)(int argc, char** argv);

typedef void (*vmLocatorSymbol_fn_t)(graal_isolatethread_t* thread);

#if defined(__cplusplus)
}
#endif
#endif
